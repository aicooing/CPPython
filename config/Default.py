# coding=utf-8


"""
基础配置
"""


class Config(object):
    HOST = '127.0.0.1'
    PORT = 9100
    DEBUG = True
    TESTING = False
