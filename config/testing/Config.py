# coding=utf-8
from ..Default import Config


class Config(Config):
    PORT = 9100
    DEBUG = False
    TESTING = True
