# coding=utf-8
import os
from .Default import Config as Default

CONFIG_PATH = os.path.abspath(os.path.dirname(__file__))
BASE_PATH = os.path.dirname(CONFIG_PATH)

# Development 开发环境
if os.access(CONFIG_PATH + '/development/Config.py', os.R_OK):
    from .development.Config import Config as Development
else:
    print('dd')
    Development = Default

# Production 生产环境
if os.access(CONFIG_PATH + '/production/Config.py', os.R_OK):
    from .production.Config import Config as Production
else:
    Production = Default

# Testing 测试环境
if os.access(CONFIG_PATH + '/testing/Config.py', os.R_OK):
    from .testing.Config import Config as Testing
else:
    Testing = Default

# 配置文件
config = {
    'DEVELOPMENT': Development,
    'PRODUCTION': Production,
    'TESTING': Testing,
    'DEFAULT': Default
}
