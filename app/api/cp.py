# coding=utf-8
from flask import jsonify, request, current_app, url_for
from . import api


@api.route('/')
def index():
    return 'hello world'
