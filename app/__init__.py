# coding=utf-8
from flask import Flask
from config import config

app = Flask(__name__)


# 初始化APP
def launch(env):
    app.config.from_object(config[env])

    from .api import api
    app.register_blueprint(api)

    app.run(host=app.config['HOST'], port=app.config['PORT'], debug=app.config['DEBUG'])
    return app


# 错误处理
@app.errorhandler(404)
@app.errorhandler(405)
def _handle_api_error(err):
    return "404"
